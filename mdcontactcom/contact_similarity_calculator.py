import argparse
from decimal import Decimal, ROUND_HALF_UP
import numpy as np
import os
import sys
import traceback

from argparse import RawTextHelpFormatter
from log import get_logger


def main(cp1: str, cp2: str, output_dir: str = None):
    """contact profile を読み込み類似性計算を行う

    Args:
        cp1 (str): contact profile file
        cp2 (str): contact profile file
        output_dir (str, optional): 出力先. Defaults to None.
    """
    if(output_dir is None):
        output_dir = os.path.join(os.getcwd(), 'contact_similarity')
    os.makedirs(output_dir, exist_ok=False)

    log_file_path = os.path.join(output_dir, "contact_similarity_calculator.log")
    lg = get_logger(name=__name__, log_file_path=log_file_path, is_stdout=True)
    lg.info('START contact_similariry_calculator')

    lg.info(
        "args: \n"
        "contact_profile_1: {cp1}\n"
        "contact_profile_2: {cp2}\n"
        "output_dir: {o}".format(cp1=cp1, cp2=cp2, o=output_dir)
    )

    output_contact_profile_diff_file = os.path.join(output_dir, 'contact_profile_diff.csv')
    output_similarity_table = os.path.join(output_dir, 'similarity_table.csv')
    lg.info(
        "output: \n"
        "contact profile diff: {diff}\n"
        "similarity table: {s}".format(diff=output_contact_profile_diff_file,
                                       s=output_similarity_table)
    )

    try:
        with open(cp1, 'r') as cp1f,\
             open(cp2, 'r') as cp2f,\
             open(output_contact_profile_diff_file, 'w') as cpdf,\
             open(output_similarity_table, 'w') as stf:

            header = True
            for cp1_row, cp2_row in zip(cp1f.readlines(), cp2f.readlines()):
                if(header):
                    cpdf.write(cp1_row)
                    stf.write(','.join(['residue_id', 'Tanimoto coefficient',
                                        'Euclidean distance\n']))
                    header = False

                else:
                    residue_id = cp1_row.split(',')[0]
                    cp1_matrix = np.array(cp1_row.split(',')[1:], dtype='float')
                    cp2_matrix = np.array(cp2_row.split(',')[1:], dtype='float')
                    lg.debug("residue id: {id}\n"
                             "cp1: {cp1}\n"
                             "cp2: {cp2}".format(id=residue_id, cp1=cp1_matrix, cp2=cp2_matrix))

                    diff = _calc_contact_profile_diff(list(cp1_matrix), list(cp2_matrix))

                    cpdf.write(",".join([residue_id] + diff) + '\n')

                    tanimoto = _calc_tanimoto(cp1_matrix, cp2_matrix)
                    tanimoto = Decimal(str(tanimoto)).quantize(Decimal('0.001'),
                                                               rounding=ROUND_HALF_UP)

                    euclidian = _calc_euclidian(cp1_matrix, cp2_matrix)
                    euclidian = Decimal(str(euclidian)).quantize(Decimal('0.001'),
                                                                 rounding=ROUND_HALF_UP)

                    stf.write(','.join([residue_id, str(tanimoto), str(euclidian)]) + '\n')

    except Exception as e:
        lg.error('{}\n{}'.format(e, traceback.format_exc()))
        sys.exit(1)
    finally:
        lg.info("END contact_similariry_calculator")
        for handle in lg.handlers[::-1]:
            handle.close()
            lg.removeHandler(handle)


def _calc_contact_profile_diff(x: list, y: list) -> list:
    """1次元のcontact profile map に対して差分配列を算出する関数

    Args:
        x (list): contact profile map
        y (list): contact profile map

    contact profile map: 1次元行列 各要素は 'float'

    Returns:
        list: contact profile map の差分行列
              1次元行列 各要素は 'str'
    """
    assert type(x) is list, 'Not List!!: {}'.format(x)
    assert type(y) is list, 'Not List!!: {}'.format(y)
    assert len(x) == len(y)

    contact_profile_diff_map = list()
    for idx in range(len(x)):

        x_data = Decimal(str(x[idx]))
        y_data = Decimal(str(y[idx]))

        if(x_data == 0 and y_data == 0):
            # 両方の contact profile の値が 0 の場合は
            # 差分の値に意味が無いとみなし空文字とする。
            contact_profile_diff_map.append('')

        else:
            contact_profile_diff_map.append(str(x_data - y_data))

    return contact_profile_diff_map


def _calc_tanimoto(x: list, y: list) -> float:
    """2配列からTANIMOTO係数を算出する関数

    Args:
        x (list/numpy.ndarray): 1次元配列
        y (list/numpy.ndarray): 1次元配列

    Returns:
        float: 配列x, y から算出された TANIMOTO係数
    """
    assert len(x) == len(y), 'len(x)={}, len(y)={}'.format(len(x), len(y))
    if(type(x) is list):
        x = np.array(x, dtype='float')
    if(type(y) is list):
        y = np.array(y, dtype='float')

    assert type(x) is np.ndarray, 'type(x): {}'.format(type(x))
    assert type(y) is np.ndarray, 'type(y): {}'.format(type(y))

    x_sum_square = (x ** 2).sum()
    y_sum_square = (y ** 2).sum()
    x_y_sum = (x * y).sum()

    denominator = (x_sum_square + y_sum_square - x_y_sum)
    if(denominator == 0):
        return 0
    else:
        return x_y_sum / denominator


def _calc_euclidian(x: list, y: list) -> float:
    """n次元の2点間のユークリッド距離を算出する関数

    Args:
        x (list/numpy.ndarray): 座標を表す1次元配列
        y (list/numpy.ndarray): 座標を表す1次元配列

        *x, y の各要素が各軸の値を示す

    Returns:
        float: 2点間 x, y のユークリッド距離
    """
    assert len(x) == len(y), 'len(x)={}, len(y)={}'.format(len(x), len(y))
    if(type(x) is list):
        x = np.array(x, dtype='float')
    if(type(y) is list):
        y = np.array(y, dtype='float')

    assert type(x) is np.ndarray, 'type(x): {}'.format(type(x))
    assert type(y) is np.ndarray, 'type(y): {}'.format(type(y))

    return np.sqrt(((x - y) ** 2).sum())


if(__name__ == '__main__'):

    parser = argparse.ArgumentParser(
        description='Read contact profile files and calculate the similarity.',
        formatter_class=RawTextHelpFormatter
    )
    parser.add_argument(
        'contact_profile_1',
        help='contact profile file',
        type=str
    )
    parser.add_argument(
        'contact_profile_2',
        help='contact profile file',
        type=str
    )

    args = parser.parse_args()
    cp1 = args.contact_profile_1
    cp2 = args.contact_profile_2
    main(cp1, cp2)
