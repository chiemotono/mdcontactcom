import argparse
import glob
import os
import psutil
import pymol
import mdtraj as md
import sys

from log import get_logger
from argparse import RawTextHelpFormatter


def convert(trajectory: str, topology: str, start: int = 0,
            stop: int = -1, step: int = 1, output_dir: str = os.getcwd(),
            disk_limit_usage: float = 95.0):
    """Trajectoryを読み込み、PDB形式に変換する

    Args:
    trajectory (str): ＭＤトラジェクトリ
    topology (str): トポロジー(分子構造)ファイル
    start (int): 読み込み開始フレーム
    stop (int): 読み込み終了フレーム
    step (int): フレームの読み込み間隔
    output_dir (str): 出力ディレクトリ
    disk_limit_usage (float): エラー終了となるディスク使用率の閾値（パーセント）
    """
    if(output_dir is None):
        output_dir = os.path.join(os.getcwd(), 'convert')
    if(not os.path.exists(output_dir)):
        os.makedirs(output_dir, exist_ok=False)
    log_file_path = os.path.join(output_dir, "convert.log")
    lg = get_logger(name=__name__, log_file_path=log_file_path, is_stdout=True)
    lg.info("START Convert tragectory file to PDB format")
    lg.info(
        "INPUT INFO: \n"
        "Trajectory: {t}\n"
        "Topology: {p}\n"
        "Start: {s}\n"
        "Stop: {e}\n"
        "Interval: {i}\n"
        "Output directory: {d}\n"
        "Disk Usage Limit: {u} %\n".format(
            t=trajectory, p=topology, s=start, e=stop, i=step, d=output_dir, u=disk_limit_usage
        )
    )
    output_pdb_file = None
    try:
        if(not os.path.exists(trajectory)):
            lg.error('Error: Not Found File: {}'.format(trajectory))
            sys.exit(1)

        elif(not os.path.exists(topology)):
            lg.error('Error: Not Found File: {}'.format(topology))
            sys.exit(1)

        elif(start < 1):
            lg.error('Error: Illegal Value for Start: {}'.format(start))
            sys.exit(1)

        elif(stop < -1 or stop == 0):
            lg.error('Error: Illegal Value for Stop: {}'.format(stop))
            sys.exit(1)

        elif(step < 1):
            lg.error('Error: Illegal Value for Interval: {}'.format(step))
            sys.exit(1)
        elif(disk_limit_usage < 0):
            lg.error('Error: Illegal Value for disk_limit_usage: {}'.format(disk_limit_usage))
            sys.exit(1)

        total = psutil.disk_usage('/').total
        used = psutil.disk_usage('/').used
        buffer_used = 1024 ** 3

        lg.debug('disk Usage INFO:\n'
                 'total = {t} B\n'
                 'used  = {u} B\n'.format(t=total, u=used))

        byte_atom_line = 80
        if(topology.endswith('-out.cms')):
            """Desmond のtrajectory 変換モジュール
            """
            pymol.pymol_argv = ['pymol', '-Qc']
            pymol.finish_launching()
            pymol.cmd.load(filename=topology)
            pymol.cmd.load_traj(filename=trajectory,
                                interval=step,
                                start=start,
                                stop=stop,
                                state=1)
            frames_num = pymol.cmd.count_frames()
            if(frames_num == 0):
                lg.warning("Read frames: {}".format(frames_num))
            else:
                lg.info("Read frames: {}".format(frames_num))
            pymol.cmd.select(name='sele', selection='all')

            atom_num = pymol.cmd.count_atoms('sel')

            # 出力ファイルによってディスク容量が不足しないように
            # ファイルサイズを予測して出力の可否を判断する。
            # ファイルサイズ = [1行辺りのバイト数] * [1フレーム当たりのATOM行数] * [フレーム数]
            predict_file_size = byte_atom_line * atom_num * frames_num

            # 出力後のディスク使用率を算出する。
            # 予測結果との差分を埋める為、buffer_used 分だけバッファを積む
            predict_disk_usage_percent = (predict_file_size + used + buffer_used) / total * 100
            lg.debug('Predicted INFO :\n'
                     'file size   = {f} B\n'
                     'buffer size = {b} B\n'
                     'disk usage  = {d:.2f} %\n'.format(
                         f=predict_file_size, b=buffer_used, d=predict_disk_usage_percent
                     ))

            if(predict_disk_usage_percent < disk_limit_usage):
                output_pdb_file = os.path.join(
                    output_dir,
                    os.path.basename(topology).replace('-out.cms', '.pdb')
                )
                pymol.cmd.save(
                    filename=output_pdb_file, selection='sele', state=0
                )

            else:
                lg.error("No sufficient space left on device")
                sys.exit(1)

        else:
            """NAMD, CHARMM, AMBER, GROMACS のtrajectory 変換モジュール
            """
            trj = md.load(trajectory, top=topology)
            frames = None
            # 配列のインデックスに変換
            start_idx = start - 1
            if(stop == -1):
                max_frames = len(trj)
                frames = trj[start_idx:max_frames:step]
            else:
                frames = trj[start_idx:stop:step]

            if(len(frames) == 0):
                lg.warning("Read frames: {}".format(len(frames)))
            else:
                lg.info("Read frames: {}".format(len(frames)))
            sel = frames.atom_slice(frames.top.select('all'))

            atom_num = trj.n_atoms

            # 出力ファイルによってディスク容量が不足しないように
            # ファイルサイズを予測して出力の可否を判断する。
            # ファイルサイズ = [1行辺りのバイト数] * [1フレーム当たりのATOM行数] * [フレーム数]
            predict_file_size = byte_atom_line * atom_num * len(frames)

            # 出力後のディスク使用率を算出する。
            # 予測結果との差分を埋める為、buffer_used 分だけバッファを積む
            predict_disk_usage_percent = (predict_file_size + used + buffer_used) / total * 100
            lg.debug('Predicted INFO :\n'
                     'file size   = {f} B\n'
                     'buffer size = {b} B\n'
                     'disk usage  = {d:.2f} %\n'.format(
                         f=predict_file_size, b=buffer_used, d=predict_disk_usage_percent
                     ))

            if(predict_disk_usage_percent < disk_limit_usage):
                output_pdb_file = os.path.join(
                    output_dir, os.path.splitext(os.path.basename(trajectory))[0] + '.pdb'
                )
                sel.save(output_pdb_file)

            else:
                lg.error("No sufficient space left on device")
                sys.exit(1)

    except Exception as e:
        lg.error(e)
        sys.exit(1)

    finally:
        lg.info(
            "OUTPUT INFO: \n"
            "PDB file: {p}\n"
            "Log file: {l}".format(p=output_pdb_file, l=log_file_path)
        )
        lg.info("END Convert trajectroy file to PDB format")
        for handle in lg.handlers[::-1]:
            handle.close()
            lg.removeHandler(handle)


if(__name__ == '__main__'):
    parser = argparse.ArgumentParser(
        description='Read trajectory and convert trajectory \
        into multi-PDB file.',
        formatter_class=RawTextHelpFormatter
    )
    parser.add_argument(
        'traj',
        help='trajectory',
        type=str
    )
    parser.add_argument(
        'topo',
        help='topology',
        type=str,
    )
    parser.add_argument(
        '--start',
        help='first frame to load from file. default: 1',
        type=int,
        default=1
    )
    parser.add_argument(
        '--stop',
        help='last frame to load from file, or -1 to load all. default: -1',
        type=int,
        default=-1
    )
    parser.add_argument(
        '--interval',
        help='Interval to take frames from file. default: 1',
        type=int,
        default=1
    )
    parser.add_argument(
        '--output_dir',
        help='Output directory. default: current directory',
        type=str,
        default=os.getcwd()
    )
    args = parser.parse_args()

    convert(
        args.traj, args.topo, args.start,
        args.stop, args.interval, args.output_dir
    )
