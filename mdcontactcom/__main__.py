import sys

from mdcontactcom import get_argparser, not_in_sub_cmd

if __name__ == '__main__':
    parser = get_argparser()
    argv = None
    if(not_in_sub_cmd(sys.argv)):
        argv = list(['run'])
        argv.extend(sys.argv[1:])
    else:
        argv = sys.argv[1:]

    args_parser = parser.parse_args(argv)
    if hasattr(args_parser, 'handler'):
        # 設定したハンドラ関数の実行
        args_parser.handler(args_parser)
    else:
        # 未知のサブコマンドの場合はヘルプを表示
        parser.print_help()
