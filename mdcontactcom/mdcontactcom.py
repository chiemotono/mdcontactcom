import argparse
import os

from argparse import RawTextHelpFormatter
from contact_profile_calculator import main as profile_main, ALL_ATOM_CONTACT_CUTOFF,\
                                               ONLY_HEAVY_ATOM_CONTACT_CUTOFF,\
                                               CA_ATOM_CONTACT_CUTOFF, CB_ATOM_CONTACT_CUTOFF
from contact_similarity_calculator import main as similarity_main
from similarity_drawer import main as drawer_main, SIMILARITY_TANIMOTO, SIMILARITY_EUCLIDEAN
from similarity_plotter import main as plotter_main
from traj2pdb import convert

OUTPUT_SYSTEM_DIR = 'outputs'
OUTPUT_SIMILARITY_DIR = 'similarity'
OUTPUT_DRAWER_DIR = 'drawer'
OUTPUT_PLOTTER_DIR = 'plotter'


def run(args):
    """システム実行関数

    Args:
        args (NameSpace): 引数解析された名前空間
    """
    trajectory1 = args.trajectory1
    topology1 = args.topology1
    trajectory2 = args.trajectory2
    topology2 = args.topology2
    region = args.region
    contact_cutoff = None
    num_process = args.num_process
    out_dist_map = args.out_dist_map

    output_dir = os.path.join(os.getcwd(), OUTPUT_SYSTEM_DIR)

    # トラジェクトリ変換処理
    trajectory1_pdb = None
    if(os.path.splitext(os.path.basename(trajectory1))[1] == '.pdb'):
        trajectory1_pdb = trajectory1

    else:
        convert_output_dir = os.path.join(output_dir, 'convert1')
        convert(trajectory=trajectory1,
                topology=topology1,
                start=1,
                stop=-1,
                step=1,
                output_dir=convert_output_dir,
                disk_limit_usage=95.0)
        if(topology1.endswith('-out.cms')):
            trajectory1_pdb = os.path.join(
                convert_output_dir,
                os.path.basename(topology1).replace('-out.cms', '.pdb'))

        else:
            trajectory1_pdb = os.path.join(
                convert_output_dir,
                os.path.splitext(os.path.basename(trajectory1))[0] + '.pdb')

    trajectory2_pdb = None
    if(os.path.splitext(os.path.basename(trajectory2))[1] == '.pdb'):
        trajectory2_pdb = trajectory2

    else:
        convert_output_dir = os.path.join(output_dir, 'convert2')
        convert(trajectory=trajectory2,
                topology=topology2,
                start=1,
                stop=-1,
                step=1,
                output_dir=convert_output_dir,
                disk_limit_usage=95.0)
        if(topology2.endswith('-out.cms')):
            trajectory2_pdb = os.path.join(
                convert_output_dir,
                os.path.basename(topology2).replace('-out.cms', '.pdb'))

        else:
            trajectory2_pdb = os.path.join(
                convert_output_dir,
                os.path.splitext(os.path.basename(trajectory2))[0] + '.pdb')

    contact_profile_list = list()
    for trajectory in [trajectory1_pdb, trajectory2_pdb]:
        file_prefix = os.path.splitext(os.path.basename(trajectory))[0]
        profile_main(trajectory=trajectory,
                     region=region,
                     contact_cutoff=contact_cutoff,
                     num_process=num_process,
                     out_dist_map=out_dist_map,
                     target_atom=args.target_atom,
                     output_dir=os.path.join(output_dir, file_prefix))
        contact_profile_list.append(os.path.join(output_dir, file_prefix, 'contact_profile.csv'))

    output_similarity_dir = os.path.join(output_dir, OUTPUT_SIMILARITY_DIR)
    similarity_main(cp1=contact_profile_list[0],
                    cp2=contact_profile_list[1],
                    output_dir=output_similarity_dir)

    drawer_main(
        contact_profile_diff_file=os.path.join(output_similarity_dir, 'contact_profile_diff.csv'),
        similarity_table_file=os.path.join(output_similarity_dir, 'similarity_table.csv'),
        trajectory=trajectory1_pdb,
        exclude_region='',
        similarity_mode=args.similarity,
        output_dir=os.path.join(output_dir, OUTPUT_DRAWER_DIR))

    plotter_main(
        similarity_table_file=os.path.join(output_similarity_dir, 'similarity_table.csv'),
        bin=50,
        exclude_region='',
        output_dir=os.path.join(output_dir, OUTPUT_PLOTTER_DIR))


def profile(args):
    """contact_profile_calculator 実行関数

    Args:
        args (NameSpace): 引数解析された名前空間
    """
    trajectory = args.trajectory
    topology = args.topology
    contact_cutoff = None
    output_dir = os.path.splitext(os.path.basename(trajectory))[0]

    # トラジェクトリ変換処理
    trajectory_pdb = None
    if(os.path.splitext(os.path.basename(trajectory))[1] == '.pdb'):
        trajectory_pdb = trajectory

    else:
        convert_output_dir = os.path.join(os.getcwd(), 'convert')
        convert(trajectory=trajectory,
                topology=topology,
                start=1,
                stop=-1,
                step=1,
                output_dir=convert_output_dir,
                disk_limit_usage=95.0)
        if(topology.endswith('-out.cms')):
            trajectory_pdb = os.path.join(
                convert_output_dir,
                os.path.basename(topology).replace('-out.cms', '.pdb'))

        else:
            trajectory_pdb = os.path.join(
                convert_output_dir,
                os.path.splitext(os.path.basename(trajectory))[0] + '.pdb')

    profile_main(trajectory=trajectory_pdb,
                 region=args.region,
                 contact_cutoff=contact_cutoff,
                 num_process=args.num_process,
                 out_dist_map=args.out_dist_map,
                 target_atom=args.target_atom,
                 output_dir=output_dir)


def similarity(args):
    """contact_similarity_calculator実行関数

    Args:
        args (NameSpace): 引数解析された名前空間
    """
    similarity_main(cp1=args.contact_profile_1,
                    cp2=args.contact_profile_2,
                    output_dir=os.path.join(os.getcwd(), OUTPUT_SIMILARITY_DIR))


def drawer(args):
    """similarity_drawer 実行関数

    Args:
        args (NameSpace): 引数解析された名前空間
    """
    trajectory = args.trajectory
    topology = args.topology
    # トラジェクトリ変換処理
    trajectory_pdb = None
    if(os.path.splitext(os.path.basename(trajectory))[1] == '.pdb'):
        trajectory_pdb = trajectory

    else:
        convert_output_dir = os.path.join(os.getcwd(), 'convert')
        convert(trajectory=trajectory,
                topology=topology,
                start=1,
                stop=-1,
                step=1,
                output_dir=convert_output_dir,
                disk_limit_usage=95.0)
        if(topology.endswith('-out.cms')):
            trajectory_pdb = os.path.join(
                convert_output_dir,
                os.path.basename(topology).replace('-out.cms', '.pdb'))

        else:
            trajectory_pdb = os.path.join(
                convert_output_dir,
                os.path.splitext(os.path.basename(trajectory))[0] + '.pdb')

    drawer_main(
        contact_profile_diff_file=args.contact_profile,
        similarity_table_file=args.similarity_table,
        trajectory=trajectory_pdb,
        exclude_region=args.noregion,
        similarity_mode=args.similarity,
        output_dir=os.path.join(os.getcwd(), OUTPUT_DRAWER_DIR))


def plotter(args):
    """similarity_plotter 実行関数

    Args:
        args (NameSpace): 引数解析された名前空間
    """
    plotter_main(
        similarity_table_file=args.similarity_table,
        bin=args.bin,
        exclude_region=args.noregion,
        output_dir=os.path.join(os.getcwd(), OUTPUT_PLOTTER_DIR))


def get_argparser():
    parser = argparse.ArgumentParser(
        description='MDContactCom System'
    )

    subparsers = parser.add_subparsers(
        dest='subparser_name'
    )

    # システム実行コマンド
    parser_run = subparsers.add_parser(
        'run',
        description="Run MDContactCom Systems",
        formatter_class=RawTextHelpFormatter,
        help='see `run -h`'
    )
    parser_run.add_argument(
        'trajectory1',
        help='trajectory file is loaded by filename extension.\n'
             'Supported trajectory formats include ".pdb", ".dcd", ".dtr", ".xtc", ".mdcrd".'
    )
    parser_run.add_argument(
        '--topology1', '-top1',
        help='topology file is loaded by filename extension.\n'
             'Supported topology formats include ".pdb", ".cms", ".psf", ".prmtop".',
        type=str,
        default=None
    )
    parser_run.add_argument(
        'trajectory2',
        help='trajectory file is loaded by filename extension.\n'
             'Supported trajectory formats include ".pdb", ".dcd", ".dtr", ".xtc", ".mdcrd".'
    )
    parser_run.add_argument(
        '--topology2', '-top2',
        help='topology file is loaded by filename extension.\n'
             'Supported topology formats include ".pdb", ".cms", ".psf", ".prmtop".',
        type=str,
        default=None
    )
    parser_run.add_argument(
        '-r', '--region',
        help='Contact profile calculation target area\n'
             'Format: (<chain>):(<resnum>) \n'
             'Multiple specifications can be specified by separating them with ","',
        default=''
    )
    parser_run.add_argument(
        '-c', '--contact_cutoff',
        help='Contact judgment threshold.\n'
             'Option "-t 0". Defaults to  {0}.\n'
             'Option "-t 1". Defaults to  {1}.\n'
             'Option "-t 2". Defaults to  {2}.\n'
             'Option "-t 3". Defaults to  {3}.\n'.format(
                 ALL_ATOM_CONTACT_CUTOFF,
                 ONLY_HEAVY_ATOM_CONTACT_CUTOFF,
                 CA_ATOM_CONTACT_CUTOFF,
                 CB_ATOM_CONTACT_CUTOFF
             ),
        type=float
    )
    parser_run.add_argument(
        '-p', '--num_process',
        help='Number of processes.(Run on multiprocessing)',
        default=1,
        type=int
    )
    parser_run.add_argument(
        '-m', '--out_dist_map',
        help='Output an intermediate file (distance matrix).',
        action='store_true'
    )
    parser_run.add_argument(
        '-t', '--target_atom',
        help='Interaction analysis target atoms.\n'
             '0: All atoms\n'
             '1: Heavy atoms (Atoms excluding hydrogen atoms)[Default]\n'
             '2: CA atoms\n'
             '3: CB atoms\n',
        default=1,
        choices=[0, 1, 2, 3],
        type=int
    )
    parser_run.add_argument(
        '-s', '--similarity',
        help='Similarity coefficient to be visualized',
        type=str,
        choices=[SIMILARITY_TANIMOTO, SIMILARITY_EUCLIDEAN],
        default=SIMILARITY_TANIMOTO
    )
    parser_run.set_defaults(handler=run)

    # contact_profile_calculator 実行コマンド
    parser_profile = subparsers.add_parser(
        'profile',
        description="Run contact_profile_calculator",
        formatter_class=RawTextHelpFormatter,
        help='see `profile -h`'
    )
    parser_profile.add_argument(
        'trajectory',
        help='trajectory file is loaded by filename extension.\n'
             'Supported trajectory formats include ".pdb", ".dcd", ".dtr", ".xtc", ".mdcrd".'
    )
    parser_profile.add_argument(
        '--topology', '-top',
        help='topology file is loaded by filename extension.\n'
             'Supported topology formats include ".pdb", ".cms", ".psf", ".prmtop".',
        type=str,
        default=None
    )
    parser_profile.add_argument(
        '-r', '--region',
        help='Contact profile calculation target area\n'
             'Format: (<chain>):(<resnum>) \n'
             'Multiple specifications can be specified by separating them with ","',
        default=''
    )
    parser_profile.add_argument(
        '-c', '--contact_cutoff',
        help='Contact judgment threshold.\n'
             'Option "-t 0". Defaults to  {0}.\n'
             'Option "-t 1". Defaults to  {1}.\n'
             'Option "-t 2". Defaults to  {2}.\n'
             'Option "-t 3". Defaults to  {3}.\n'.format(
                 ALL_ATOM_CONTACT_CUTOFF,
                 ONLY_HEAVY_ATOM_CONTACT_CUTOFF,
                 CA_ATOM_CONTACT_CUTOFF,
                 CB_ATOM_CONTACT_CUTOFF
             ),
        type=float
    )
    parser_profile.add_argument(
        '-p', '--num_process',
        help='Number of processes.(Run on multiprocessing)',
        default=1,
        type=int
    )
    parser_profile.add_argument(
        '-m', '--out_dist_map',
        help='Output an intermediate file (distance matrix).',
        action='store_true'
    )
    parser_profile.add_argument(
        '-t', '--target_atom',
        help='Interaction analysis target atoms.\n'
             '0: All atoms\n'
             '1: Heavy atoms (Atoms excluding hydrogen atoms)[Default]\n'
             '2: CA atoms\n'
             '3: CB atoms\n',
        default=1,
        choices=[0, 1, 2, 3],
        type=int
    )
    parser_profile.set_defaults(handler=profile)

    # contact_similarity_calculator
    parser_similarity = subparsers.add_parser(
        'similarity',
        description="Run contact_similarity_calculator",
        formatter_class=RawTextHelpFormatter,
        help='see `similarity -h`'
    )
    parser_similarity.add_argument(
        'contact_profile_1',
        help='contact profile file',
        type=str
    )
    parser_similarity.add_argument(
        'contact_profile_2',
        help='contact profile file',
        type=str
    )
    parser_similarity.set_defaults(handler=similarity)

    # similarity_drawer
    parser_drawer = subparsers.add_parser(
        'drawer',
        description="Run similarity_drawer",
        formatter_class=RawTextHelpFormatter,
        help='see `drawer -h`'
    )
    parser_drawer.add_argument(
        'contact_profile',
        help='contact profile diff file.'
    )
    parser_drawer.add_argument(
        'similarity_table',
        help='Similarity table file.'
    )
    parser_drawer.add_argument(
        'trajectory',
        help='trajectory file is loaded by filename extension.\n'
             'Supported trajectory formats include ".pdb", ".dcd", ".dtr", ".xtc", ".mdcrd".'
    )
    parser_drawer.add_argument(
        '--topology', '-top',
        help='topology file is loaded by filename extension.\n'
             'Supported topology formats include ".pdb", ".cms", ".psf", ".prmtop".',
        type=str,
        default=None
    )
    parser_drawer.add_argument(
        '--noregion', '-n',
        help='Exclude residues.\n'
             'Format: (<chain>):(<resnum>) \n'
             'Multiple specifications can be specified by separating them with ","',
        default=''
    )
    parser_drawer.add_argument(
        '-s', '--similarity',
        help='Similarity coefficient to be visualized',
        type=str,
        choices=[SIMILARITY_TANIMOTO, SIMILARITY_EUCLIDEAN],
        default=SIMILARITY_TANIMOTO
    )
    parser_drawer.set_defaults(handler=drawer)

    # similarity_plotter
    parser_plotter = subparsers.add_parser(
        'plotter',
        description="Run similarity_plotter",
        formatter_class=RawTextHelpFormatter,
        help='see `plotter -h`'
    )
    parser_plotter.add_argument(
        'similarity_table',
        help='similarity table file',
        type=str
    )
    parser_plotter.add_argument(
        '--bin', '-b',
        help='Display width of graph scale. (Defalt 50)',
        type=int,
        default=50
    )
    parser_plotter.add_argument(
        '--noregion', '-n',
        help='Exclude residues.\n'
             'Format: (<chain>):(<resnum>) \n'
             'Multiple specifications can be specified by separating them with ","',
        default=''
    )
    parser_plotter.set_defaults(handler=plotter)

    return parser


def not_in_sub_cmd(argv: list) -> bool:
    """標準入力にサブコマンドが指定されているかを判断する関数

    Args:
        argv (list): コマンド実行時の標準入力: sys.argv

    Returns:
        bool: 指定のサブコマンドが含まれていない場合 TRUE
    """
    if(len(argv) < 2):
        return True

    subcommands = ['-h', 'run', 'profile', 'similarity', 'drawer', 'plotter']
    return argv[1] not in subcommands
